const morse 		= require('morse');	

module.exports = {
    visualizacionPrtada: function(req, res) {
        try {
            let respBody = '{"code": 200, "response": "Examen Técnico - MercadoLibre S.R.L"}';
            let message = JSON.parse(respBody);
            res.status(200).send(message);
            return;
        }
        catch (e) {
            var respBody = '{"code": 400, "error": "' + e.error + '","description": "' + e.message + '"}';
            let message = JSON.parse(respBody);
            res.status(400).send(message);
            return;
        }
    },
    translateBits2Morse: function(req, res) {
        try {
            let binaryText = req.body.text;
            let morseText = decodeBits2Morse(binaryText);
            let respBody = '{"code": 200, "response": "' + morseText + '"}';
            let message = JSON.parse(respBody);
            res.status(200).send(message);
            return;
        }
        catch (e) {
            var respBody = '{"code": 400, "error": "' + e.error + '","description": "' + e.message + '"}';
            let message = JSON.parse(respBody);
            res.status(400).send(message);
            return;
        }
    },
    translate2Human: function(req, res) {

        try {
			let morseText = req.body.text;
            validaMose(morseText);
			let humanText = morse.decode(morseText);
			let respBody = '{"code": 200, "response": "' + humanText + '"}';
			let message = JSON.parse(respBody);
			res.status(200).send(message);
			return;
        }
        catch (e) {
			var respBody = '{"code": 400, "error": "' + e.error + '","description": "' + e.message + '"}';
			let message = JSON.parse(respBody);
            res.send(message);
			return;
        }


    },
    translate2Morse: function(req, res) {
        try {
				let encode_text = req.body.text;
				let condict_text = morse.encode(encode_text);
				condict_text = condict_text.trim();
				let respBody = '{"code": 200, "response": "' + condict_text + '"}';
                let message = JSON.parse(respBody);
    			res.status(200).send(message);
				return
        }
        catch (e) {
				var respBody = '{"code": 400, "error": "' + e.error + '","description": "' + e.message + '"}';
                respuesta = JSON.parse(respBody);
                res.send(respuesta);
				return
        }
    }
};


var validaMose = function(strMorse) {
    var strTmpMorse = strMorse;

    strTmpMorse = strTmpMorse.replace(/\./g, function(x) {
        return "";
    });
    
    strTmpMorse = strTmpMorse.replace(/-/g, function(x) {
        return "";
    });

    if (strTmpMorse.trim().length > 0) {
        throw new Error('El codigo morse es incoorecto.');
    }

}


var validaBinario = function(strBinario) {

    var strTmpBinario = strBinario;

    strTmpBinario = strTmpBinario.replace(/0/gi, function(x) {
        return "";
    });

    strTmpBinario = strTmpBinario.replace(/1/gi, function(x) {
        return "";
    });

    if (strTmpBinario != "") {
        throw new Error('El codigo binario es incoorecto.');
    }

}

var decodeBits2Morse = function(strBinario) {
    //var strBinario = codeBinary;

    validaBinario(strBinario);


    strBinario = strBinario.trim().replace(/0000000/g, function(x) {
        return "| |";
    });
    strBinario = strBinario.trim().replace(/000000/g, function(x) {
        return "| |";
    });
    strBinario = strBinario.trim().replace(/00000/g, function(x) {
        return "| |";
    });

    strBinario = strBinario.trim().replace(/0000/g, function(x) {
        return "| |";
    });
    strBinario = strBinario.trim().replace(/000/g, function(x) {
        return "|";
    });
    strBinario = strBinario.trim().replace(/00/g, function(x) {
        return "|";
    });
    strBinario = strBinario.trim().replace(/0/g, function(x) {
        return "|";
    });

    var codMorese = "";
    var array = strBinario.split("|");
    array.forEach(function(element) {

        if (element.charAt(0) != "1") {

            codMorese += element
        } else {

            if (parseInt(element) > 1111) {
                codMorese += "-"
            } else if (parseInt(element) <= 1111) {
                codMorese += "."
            }
        }
    });
    return codMorese;
}
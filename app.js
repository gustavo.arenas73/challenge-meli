'use strict';
var translate = require('./functions/translate');
const express       = require('express');
const bodyParser    = require('body-parser');
const Buffer        = require('safe-buffer').Buffer;
const app           = express();


app.use(bodyParser.json());

app.get('/', function (req, res) {
	translate.visualizacionPrtada(req, res);
});

app.post('/translate/bit2morse', function (req, res) {
	translate.translateBits2Morse(req, res);
});

app.post('/translate/2text', function (req, res) {
	translate.translate2Human(req, res);
});

app.post('/translate/2morse', function (req, res) {
	translate.translate2Morse(req, res);
});

if (module === require.main) {
  const PORT = process.env.PORT || 8080;
  app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
    console.log('Press Ctrl+C to quit.');
  });
}

module.exports = app;

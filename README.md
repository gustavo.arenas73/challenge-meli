                            Examen Técnico - MercadoLibre S.R.L

Como todo lo “retro” últimamente está de moda, en MELI nos propusimos utilizar el telégrafo
para comunicaciones entre distintos equipos utilizando código MORSE. En este contexto
nuestro equipo se propone diseñar una solución que permita identificar los mensajes de
cada uno de los emisores dentro de la empresa.
Un problema implícito para la interpretación de los mensajes en MORSE, es la velocidad de
trasmisión de los mismos. Naturalmente la velocidad de transmisión del código varía
ligeramente a lo largo del mensaje ya que, es enviada por un operador humano.
Veamos un ejemplo, el texto “HOLA MELI" en MORSE sería el siguiente:
.... --- .-.. .- -- . .-.. ..
Una transmisión en bits podría representarse de la siguiente forma (considerando el bit 1
como un pulso y el 0 como pausa):
0000000011011011001110000011111100011111100111111000000011101111111101110
1110000000110001111110000011111100111111000000011000011011111111011101110
0000011011100000000000
Como se puede ver, esta transmisión es generalmente precisa de acuerdo con el estándar,
pero algunos puntos, trazos y pausas son un poco más cortos o un poco más largos que los
demás en el mismo mensaje.
Tener en cuenta que no necesariamente la frecuencia de envío es constante (dado que es
un operador humano) pero sí se considera que la representación es consistente durante
todo el mensaje. Es decir, si un operador es lento o rápido, seguirá siendo lento o rápido a
lo largo del envío de ese mensaje. Ejemplificando, si los puntos para cierto operador son de
largo menor o igual a 5 bits, no pasará durante el envío que desee representar un trazo de
largo 4 o 5. El mismo concepto aplica a las pausas para la interpretación de la separación
entre letras, palabras y finalización del mensaje.
Se entiende aquí que una pausa prolongada o el ingreso de un “full stop” (.-.-.-) indica el fin
del mensaje. No es requisito soportar ambos métodos de finalización.
Se pide implementar en cualquier lenguaje de programación :
1. Una función decodeBits2Morse que dada una secuencia de bits, retorne un
string con el resultado en MORSE.
2. Una función translate2Human que tome el string en MORSE y retorne un string
legible por un humano. Se puede utilizar la tabla debajo como referencia.

Nota:
● Si desea utilizar un repositorio, de preferencia que sea privado (ej: bitbucket, gitlab)
● Se deja a libre elección, el modo de transformar un input físico a bits.
Bonus:
1. Diseñar una API que permita traducir texto de MORSE a lenguaje humano y
visceversa.
2. Hostear la API en un cloud público (como app engine o cloud foundry) y enviar la
URL para consulta
Ejemplo:
$ curl -X POST "http://meli.com/translate/2text" -d "{text: '.... --- .-.. .- -- . .-.. ..'}"
{ code:200, response: 'HOLA MELI'}
$ curl -X POST "http://meli.com/translate/2morse" -d "{text: 'HOLA MELI'}"
{ code:200, response: '.... --- .-.. .- -- . .-.. ..'}



                                Repositorio GITLAB

Clone with HTTPS
https://gitlab.com/gustavo.arenas73/challenge-meli.git


                                Respositorio GCloud

URL
https://console.cloud.google.com/functions/list?project=meli-233200&hl=es


Cloud FunCtion
https://console.cloud.google.com/functions/list?project=meli-233200&hl=es




                                IMPLEMENTACION

REST API - LOCAL 

curl -X POST \
  http://localhost:8080/translate/bit2morse \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 708b7a06-03b7-4553-ac4a-f2876fdc4ccf' \
  -H 'cache-control: no-cache' \
  -d '{
	"text":"00000000110110110011100000111111000111111001111110000000111011111111011101110000000110001111110000111111001111110000000110000110111111110111011100000011011100000000000"
}'
--------------------------


curl -X POST \
  http://localhost:8080/translate/2morse \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: fb619963-eaeb-4d3e-bfbb-c190f74e6bb0' \
  -H 'cache-control: no-cache' \
  -d '{
	"text":  "HOLA MELI"
}'
-------------------------------


curl -X POST \
  http://localhost:8080/translate/2text \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 744098f9-3101-4a33-8626-89516ec36787' \
  -H 'cache-control: no-cache' \
  -d '{
	"text":  ".... --- .-.. .- ....... -- . .-.. .."
}'
-------------------------------




Google Cloud

curl -X POST \
  https://us-central1-meli-233200.cloudfunctions.net/translate2Human \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: b94882d7-a05c-4f9d-b3d5-6587b9f9b49e' \
  -H 'cache-control: no-cache' \
  -d '{
	"text":  " .... --- .-.. .- -- . .-.. ..  "
}'

-------------------------------


curl -X POST \
  https://us-central1-meli-233200.cloudfunctions.net/translate2Morse \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 4fe178ee-855a-43c9-812d-bde51e5d98cd' \
  -H 'cache-control: no-cache' \
  -d '{
	"text":"HOLA MELI"
}'
----------------------------------
